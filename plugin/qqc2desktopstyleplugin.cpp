/*
    SPDX-FileCopyrightText: 2017 Marco Martin <mart@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "qqc2desktopstyleplugin.h"
#include "kquickstyleitem_p.h"
#include "kpropertywriter_p.h"

#include <QQmlEngine>
#include <QQmlContext>
#include <QQuickItem>


void QQc2DesktopStylePlugin::registerTypes(const char *uri)
{
    Q_ASSERT(QLatin1String(uri) == QLatin1String("org.kde.qqc2desktopstyle.private"));

    qmlRegisterType<KQuickStyleItem>(uri, 1, 0, "StyleItem");
    qmlRegisterType<KPropertyWriter>(uri, 1, 0, "PropertyWriter");
#if QT_VERSION < QT_VERSION_CHECK(5, 14, 0)
    qmlRegisterType<KQuickPadding>();
#else
    qmlRegisterAnonymousType<KQuickPadding>(uri, 1);
#endif
    qmlProtectModule(uri, 2);
}

#include "moc_qqc2desktopstyleplugin.cpp"

